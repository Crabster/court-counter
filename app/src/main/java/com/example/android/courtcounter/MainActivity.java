package com.example.android.courtcounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA = 0;
    int scoreTeamB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Increase the score for Teams.
     */
    public void addScoreForTeams(View v) {
        switch (v.getId()) {
            case R.id.team_a_one:
                scoreTeamA += 1;
                displayForTeamA(scoreTeamA);
                break;
            case R.id.team_a_two:
                scoreTeamA += 2;
                displayForTeamA(scoreTeamA);
                break;
            case R.id.team_a_three:
                scoreTeamA += 3;
                displayForTeamA(scoreTeamA);
                break;
            case R.id.team_b_one:
                scoreTeamB += 1;
                displayForTeamB(scoreTeamB);
                break;
            case R.id.team_b_two:
                scoreTeamB += 2;
                displayForTeamB(scoreTeamB);
                break;
            case R.id.team_b_three:
                scoreTeamB += 3;
                displayForTeamB(scoreTeamB);
                break;
        }
    }

    /**
     * Resets all scores.
     */
    public void reset(View v) {
        displayForTeamA(scoreTeamA = 0);
        displayForTeamB(scoreTeamB = 0);
    }

    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Displays the given score for Team B.
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

}